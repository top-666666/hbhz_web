# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/19 19:06
@Author : 李佳豪
@File   : VarConfig.py.py
"""
# 存储全局的变量
import os

# 项目根目录
projectPath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 截图目录
exceptionPath = projectPath + r'\exceptionPictures'

# 驱动存放路径， 需要自己根据自己电脑的驱动为止修改
# iePath = ''
# chromePath = ''
# fireFox = ''

# excel文件存放路径
# excelPath = projectPath + r'\testData\126mailSend.xlsx'
# loh文件存放路径
logPath = projectPath + '\\logs\\'
# 测试用例部分列对应的列号
testCase_testCaseName = 2  # 用例名称
testCase_testStepName = 4  # 步骤名
testCase_testIsExecute = 5  # 是否执行
testCase_testRunEndTime = 6  # 执行结束时间
testCase_testResult = 7  # 结果

# 用例步骤对应的列号
testStep_testNum = 1  # 序号
testStep_testStepDescribe = 2  # 测试步骤描述
testStep_keyWord = 3  # 关键字
testStep_elementBy = 4  # 操作
testStep_elementLocator = 5  # 操作元素定位表达式
testStep_operateValue = 6  # 操作值
testStep_testRunTime = 7  # 测试执行时间
testStep_testResult = 8  # 测试结果
testStep_testErrorInfo = 9  # 错误信息
testStep_testErrorPic = 10  # 错误截图

if __name__ == '__main__':
    print(projectPath)
    print(exceptionPath)
    # print(chromePath)
