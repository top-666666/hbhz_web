# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/14 23:40
@Author : 李佳豪
@File   : config.py
"""
from configparser import ConfigParser
import os


class Config:
    path_dir = str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

    def __init__(self):
        """
        初始化
        """
        self.config = ConfigParser()
        self.conf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini')

        if not os.path.exists(self.conf_path):
            raise FileNotFoundError("请确保配置文件存在！")

        self.config.read(self.conf_path, encoding='utf-8')

        # 数据库服务信息
        self.mysql_host = self.get_conf('mysql', 'host')
        self.mysql_port = self.get_conf('mysql', 'port')
        self.mysql_user = self.get_conf('mysql', 'user')
        self.mysql_password = self.get_conf('mysql', 'password')
        self.mysql_database = self.get_conf('mysql', 'database')
        self.mysql_charset = self.get_conf('mysql', 'charset')

    def get_conf(self, title, value):
        """
        配置文件读取
        :param title: 配置组
        :param value: 配置项
        :return:
        """
        return self.config.get(title, value)


if __name__ == '__main__':
    print(Config().mysql_host)
    print(Config().conf_path)
