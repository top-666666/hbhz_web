# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/26 9:56
@Author : 李佳豪
@File   : run.py
"""

import unittest
from testsuites.suite_demo import suite
from XTestRunner import HTMLTestRunner

if __name__ == '__main__':
    # unittest.TextTestRunner().run(suite)

    filename = 'reports/result.html'
    with open(filename, 'wb+') as f:
        # filename = 'reports/result.html'
        # 注意这里要用wb的形式去写
        runner = HTMLTestRunner(stream=f,
                                title='测试报告',
                                # blacklist=["base"],
                                description='互帮互助自动化测试',
                                tester='邢凯', )
        runner.run(suite)
        """
        blacklist是黑名单-->只有黑名单中的用例不会被执行
        whitelist是白名单-->只会执行白名单中的用例
        使用方法：
            @label("base")
            def test_01(self):
                print("用例1")
        在用例上分添加@label("名字")，可以标记
        然后在HTMLTestRunner参数中添加@label标记的名字
        例如：
        blacklist=["名字"]
        """
