# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/26 9:47
@Author : 李佳豪
@File   : suite_demo.py
"""
import unittest

from testCases.Test126SendMailWithAttachment2 import Test126SendMailWithAttachment2
from testCases.advertisementphoto.Testhelpaddadvertisementphoto import TestAddAdvertisementPhoto
from testCases.rich_text.Testhelpaddrichtext import TestAddRichText

suite = unittest.TestLoader().loadTestsFromTestCase(TestAddRichText)


# 第一种执行方式：加载单个用例
# suite = unittest.TestSuite()
# suite.addTest(TestUnittest('test_two'))
# suite.addTest(TestUnittest('test_three'))
# unittest.TextTestRunner().run(suite)

# 第二种执行方式 加载测试类
# suite = unittest.TestLoader().loadTestsFromTestCase(TestUnittest)
# unittest.TextTestRunner().run(suite)

# 第三种执行方式 加载测试文件
# suite = unittest.defaultTestLoader.discover(start_dir='./case', pattern='test_unittest.py')
# unittest.TextTestRunner().run(suite)