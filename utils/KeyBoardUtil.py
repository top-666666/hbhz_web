# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/21 16:17
@Author : 李佳豪
@File   : KeyBoardUtil.py
"""
import time

import win32api
import win32con


class KeyBoardKeys(object):
    """
    模拟键盘
    """
    # 键盘编码
    vk_code = {
        'enter': 0x0D,
        'tab': 0x09,
        'ctrl': 0x11,
        'v': 0x56,
    }

    @staticmethod
    def key_down(key_name):
        """
        模拟按下键
        :param key_name:
        :return:
        """
        try:
            win32api.keybd_event(KeyBoardKeys.vk_code[key_name], 0, 0, 0)
        except Exception as e:
            raise e

    @staticmethod
    def key_up(key_name):
        """
        释放键
        :param key_name:
        :return:
        """
        try:
            win32api.keybd_event(KeyBoardKeys.vk_code[key_name], 0, win32con.KEYEVENTF_KEYUP, 0)
        except Exception as e:
            raise e

    @staticmethod
    def one_key(key):
        """
        模拟单个按键
        :return:
        """
        try:
            KeyBoardKeys.key_down(key)
            KeyBoardKeys.key_up(key)
        except Exception as e:
            raise e

    @staticmethod
    def two_keys(key1, key2):
        """
        模拟组合按键
        :param key1:
        :param key2:
        :return:
        """
        try:
            KeyBoardKeys.key_down(key1)
            KeyBoardKeys.key_down(key2)
            KeyBoardKeys.key_up(key1)
            KeyBoardKeys.key_up(key2)
        except Exception as e:
            raise e


if __name__ == '__main__':
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from time import sleep

    driver = webdriver.Firefox()
    driver.get('http://www.baidu.com')
    driver.find_element(By.ID, 'kw').send_keys('python')
    KeyBoardKeys.one_key('enter')
    sleep(3)
    driver.quit()
