# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/21 0:56
@Author : 李佳豪
@File   : WaitUntil.py
"""
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class WaitUnit(object):
    def __init__(self, driver):
        self.by_dic = {
            'id': By.ID,
            'name': By.NAME,
            'xpath': By.XPATH,
            'link_text': By.LINK_TEXT,
            'class_name': By.CLASS_NAME,
            'css_selector': By.CSS_SELECTOR,
            'tag_name': By.TAG_NAME,
            'partial_link_text': By.PARTIAL_LINK_TEXT,
        }
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    def presence_of_element_located(self, by, locator):
        """
        显示等待某个元素出现在dom中，不一定可见，存在返回元素对象
        :param by:
        :param locator:
        :return:
        """
        try:
            if by.lower() in self.by_dic:
                self.wait.until(EC.presence_of_element_located((self.by_dic[by.lower()], locator)))
            else:
                raise TypeError('未找到定位方式,请确保定位方式正确')
        except Exception as e:
            raise e

    def frame_to_be_available_and_switch_to_it(self, by, locator):
        """
        检查frame是否存在，存在就切换到frame中
        :param by:
        :param locator:
        :return:
        """
        try:
            if by.lower() in self.by_dic:
                self.wait.until(EC.frame_to_be_available_and_switch_to_it((self.by_dic[by.lower()], locator)))
            else:
                raise TypeError('未找到定位方式,请确保定位方式正确')
        except Exception as e:
            raise e

    def visibility_of_element_located(self, by, locator):
        """
        显示等待页面元素出现在dom中， 并且可见， 存在则返回该元素对象
        :param by:
        :param locator:
        :return:
        """
        try:
            if by.lower() in self.by_dic:
                self.wait.until(EC.visibility_of_element_located((self.by_dic[by.lower()], locator)))
            else:
                raise TypeError('未找到定位方式,请确保定位方式正确')
        except Exception as e:
            raise e


if __name__ == '__main__':
    from utils.ObjectMap import *
    from selenium import webdriver

    driver = webdriver.Firefox()
    driver.get('https://mail.126.com')
    wait = WaitUnit(driver)
    wait.frame_to_be_available_and_switch_to_it('xpath', "//div[@id='loginDiv']/iframe")
    wait.visibility_of_element_located('xpath', "//input[@name='email']")
    uname = get_element(driver, 'xpath', "//input[@name='email']")
    uname.send_keys('python')
    driver.quit()
