# -*- coding: utf-8 -*-
"""
@Time   : 2023/7/25 19:53
@Author : 李佳豪
@File   : random_util.py
"""
import random
import string


class RandomUtil:
    @staticmethod
    def generate_random_number(start, end):
        """生成指定范围内的随机整数"""
        return random.randint(start, end)

    @staticmethod
    def generate_random_string(length):
        """生成指定长度的随机字符串"""
        letters = string.ascii_letters + string.digits
        return ''.join(random.choice(letters) for _ in range(length))

    @staticmethod
    def generate_unique_random_numbers(start, end, count):
        """生成指定范围内指定数量的不重复随机整数"""
        numbers = random.sample(range(start, end + 1), count)
        return numbers

    @staticmethod
    def generate_unique_random_strings(length, count):
        """生成指定长度指定数量的不重复随机字符串"""
        strings = set()
        while len(strings) < count:
            random_string = RandomUtil.generate_random_string(length)
            strings.add(random_string)
        return list(strings)


if __name__ == '__main__':
    """ 1-9的随机整数 """
    print(RandomUtil.generate_random_number(1, 10))

    """ 生成5位指定长度的随机字符串 """
    print(RandomUtil.generate_random_string(5))

    """生成1-20 5位 指定范围内指定数量的不重复随机整数"""
    print(RandomUtil.generate_unique_random_numbers(1, 20, 5))

    """生成指定长度指定数量的不重复随机字符串"""
    print(RandomUtil.generate_unique_random_strings(5, 1))
