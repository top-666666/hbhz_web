# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/21 1:29
@Author : 李佳豪
@File   : log.py
"""
import logging
import time
from config.VarConfig import *


class Logger(object):
    """
    封装日志模块
    """

    def __init__(self, logger, cmd_level=logging.INFO, file_level=logging.INFO):
        """

        :param logger:
        :param cmd_level:
        :param file_level:
        """
        try:
            self.logger = logging.getLogger(logger)
            self.logger.setLevel(logging.DEBUG)  # 设置日志输出的默认级别
            # 日志输出格式
            fmt = logging.Formatter('%(asctime)s - %(filename)s:[%(lineno)s] - [%(levelname)s] - %(message)s')
            # 日志文件名称
            # self.LogFileName = os.path.join(conf.log_path, "{0}.log.txt".format(time.strftime("%Y-%m-%d")))# %H_%M_%S
            curr_time = time.strftime("%Y-%m-%d")
            self.log_file_name = logPath + curr_time + '.txt'
            # 设置控制台输出
            sh = logging.StreamHandler()
            sh.setFormatter(fmt)
            sh.setLevel(cmd_level)  # 日志级别

            # 设置文件输出
            fh = logging.FileHandler(self.log_file_name)
            fh.setFormatter(fmt)
            fh.setLevel(file_level)  # 日志级别
            self.logger.addHandler(sh)
            self.logger.addHandler(fh)
        except Exception as e:
            raise e


if __name__ == '__main__':
    logger = Logger("fox", cmd_level=logging.DEBUG, file_level=logging.DEBUG)
    logger.logger.debug("debug")
    logger.logger.log(logging.ERROR, '%(module)s %(info)s', {'module': 'log日志', 'info': 'error'})  # ERROR,log日志 error
