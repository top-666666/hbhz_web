# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/21 14:58
@Author : 李佳豪
@File   : ClipboardUtil.py
"""
import win32clipboard as w
import win32con


# pip install pywin32


class Clipboard(object):

    @staticmethod
    def get_text():
        """
        获取剪切板的内容
        :return:
        """
        try:
            # 打开剪切板
            w.OpenClipboard()
            # 读取数据
            value = w.GetClipboardData(win32con.CF_TEXT)
            # 关闭剪切板
            w.CloseClipboard()
        except Exception as e:
            raise e
        else:
            return value

    @staticmethod
    def set_text(value):
        """
        设置剪切板内容
        :param value:
        :return:
        """
        try:
            w.OpenClipboard()  # 打开剪切板
            w.EmptyClipboard()  # 清空剪切板
            w.SetClipboardData(win32con.CF_UNICODETEXT, value)  # 设置内容
            w.CloseClipboard()  # 关闭
        except Exception as e:
            raise e


if __name__ == '__main__':
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from time import sleep

    value = 'python'
    driver = webdriver.Firefox()
    driver.get('http://www.baidu.com')
    query = driver.find_element(By.ID, 'kw')
    Clipboard.set_text(value)
    cl_value = Clipboard.get_text()
    query.send_keys(cl_value.decode('utf-8'))
    sleep(3)
    driver.quit()
