# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/19 18:55
@Author : 李佳豪
@File   : ParseExcel.py
"""
from config.VarConfig import *

from openpyxl import load_workbook

from datetime import datetime


class ParseExcel:
    """
    解析excel文件的封装
    """

    # def __init__(self):
    #     # 加载excel文件到内存
    #     self.wb = load_workbook(excelPath)

    @staticmethod
    def get_row_value(excelPath, sheet_name, raw_no):
        """
        获取某一行的数据
        :param sheet_name: 表名
        :param raw_no: 列号
        :return: 列表
        """
        wb = load_workbook(excelPath)
        sh = wb[sheet_name]
        row_value_list = []
        # 遍历每一列的数据，从第二列开始
        for y in range(2, sh.max_column + 1):
            # 使用坐标
            value = sh.cell(raw_no, y).value
            row_value_list.append(value)
        wb.close()
        return row_value_list

    @staticmethod
    def get_column_value(excelPath, sheet_name, col_no):
        """
        获取某一列的值
        :param sheet_name: 表名
        :param col_no: 行号
        :return: 列表
        """
        wb = load_workbook(excelPath)
        sh = wb[sheet_name]
        col_value_list = []
        for x in range(2, sh.max_row + 1):
            value = sh.cell(x, col_no).value
            col_value_list.append(value)
        wb.close()
        return col_value_list

    @staticmethod
    def get_cell_of_value(excelPath, sheet_name, row_no, col_no):
        """
        获取某一个单元格的数据
        :param sheet_name:
        :param row_no:
        :param col_no:
        :return: 字符串
        """
        wb = load_workbook(excelPath)
        sh = wb[sheet_name]
        value = sh.cell(row_no, col_no).value
        wb.close()
        return value

    @staticmethod
    def write_cell(excelPath, sheet_name, row_no, col_no, value):
        """
        向某个单元格写入数据
        :param sheet_name: 表名
        :param row_no: 行号
        :param col_no: 列号
        :param value: 要写入的值
        :return: 无
        """
        wb = load_workbook(excelPath)
        sh = wb[sheet_name]
        sh.cell(row_no, col_no).value = value
        wb.save(excelPath)
        wb.close()

    @staticmethod
    def write_current_time(excelPath, sheet_name, row_no, col_no):
        """
        向某个单元格写入时间
        :param sheet_name: 表名
        :param row_no: 行号
        :param col_no: 列号
        :return: 无
        """
        wb = load_workbook(excelPath)
        sh = wb[sheet_name]
        time = datetime.now()
        current_time = time.strftime('%Y:%m:%d %H:%M:%S')
        sh.cell(row_no, col_no).value = current_time
        wb.save(excelPath)
        wb.close()

    @staticmethod
    def write_test_result(excelPath, sheet_name, row_no, result, error_info=None, error_pic=None):
        """
        写入测试结果
        :param sheet_name:  表名
        :param row_no: 行号
        :param result: 写入的值
        :param error_info: 错误信息
        :param error_pic: 错误图片路径
        :return:
        """
        ParseExcel.write_current_time(excelPath, sheet_name, row_no, testStep_testRunTime)
        ParseExcel.write_cell(excelPath, sheet_name, row_no, testStep_testResult, result)
        if error_info and error_pic:
            ParseExcel.write_cell(excelPath, sheet_name, row_no, testStep_testErrorInfo, error_info)
            ParseExcel.write_cell(excelPath, sheet_name, row_no, testStep_testErrorPic, error_pic)
        else:
            ParseExcel.write_cell(excelPath, sheet_name, row_no, testStep_testErrorInfo, '')
            ParseExcel.write_cell(excelPath, sheet_name, row_no, testStep_testErrorPic, '')


if __name__ == '__main__':
    excelPath = projectPath + r'\testData\126mailSend.xlsx'
    # 获取Sheet1表，第一行（忽略了第一列数据）
    row = ParseExcel.get_row_value(excelPath, 'Sheet1', 1)
    print(row)
    # 获取Sheet2表，第一列（忽略了第一行数据）
    col = ParseExcel.get_column_value(excelPath, 'Sheet2', 1)
    print(col)
    # 获取Sheet1表，第1行第2个数据
    cell = ParseExcel.get_cell_of_value(excelPath, 'Sheet1', 1, 2)
    print(cell)
    # 获取Sheet3表，第1行第1列写入数据（注：写入时，请关闭对应xlsx文件，否则会提示无权限）
    # ParseExcel().write_cell('Sheet3', 1, 1, '李佳豪大帅比')
    # # 获取Sheet3表，第1行第2列写入数据（注：写入时，请关闭对应xlsx文件，否则会提示无权限）
    # ParseExcel().write_current_time('Sheet3', 1, 2)
