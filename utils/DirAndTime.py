# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/21 9:32
@Author : 李佳豪
@File   : DirAndTime.py
"""
from datetime import datetime, date
from config.VarConfig import *


class DirAndTime(object):
    @staticmethod
    def get_current_data():
        """
        获取当前日期（2023-08-21）
        :return:
        """
        try:
            current_data = date.today()
        except Exception as e:
            raise e
        else:
            return str(current_data)

    @staticmethod
    def get_current_time():
        """
        获取当前时间(09_38_38)
        :return:
        """
        try:
            time = datetime.now()
            current_time = time.strftime('%H_%M_%S')
        except Exception as e:
            raise e
        else:
            return current_time

    @staticmethod
    def create_picture_path():
        """
        创建图片存放路径
        :return:
        """
        try:
            picture_path = os.path.join(exceptionPath, DirAndTime.get_current_data())
            if not os.path.exists(picture_path):
                os.makedirs(picture_path)  # 生成多级目录
        except Exception as e:
            raise e
        else:
            return picture_path


if __name__ == '__main__':
    print(DirAndTime.get_current_data())
    print(DirAndTime.get_current_time())
    print(DirAndTime.create_picture_path())
