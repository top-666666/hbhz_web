# -*- coding: utf-8 -*-
"""
@Time   : 2023/8/21 0:40
@Author : 李佳豪
@File   : ObjectMap.py
"""
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common import TimeoutException, NoSuchElementException
import logging


def get_element(driver, by, locator):
    """
    查找单一元素
    :param driver:
    :param by:
    :param locator:
    :return: 元素对象
    """
    try:
        element = WebDriverWait(driver, 10).until(lambda x: x.find_element(by, locator))
    except NoSuchElementException:
        logging.error("元素未找到！")
    except Exception as e:
        raise e
    else:
        return element


def get_elements(driver, by, locator):
    """
    获取一组元素
    :param driver:
    :param by:
    :param locator:
    :return: 一组元素对象
    """
    try:
        elements = WebDriverWait(driver, 10).until(lambda x: x.find_element(by, locator))
    except NoSuchElementException:
        print("元素未找到！")
    except Exception as e:
        raise e
    else:
        return elements


if __name__ == '__main__':
    from selenium import webdriver
    import time

    driver = webdriver.Firefox()
    driver.get('https://mail.126.com')
    time.sleep(5)
    driver.switch_to.frame(get_element(driver, 'xpath', "//div[@id='loginDiv']/iframe"))
    username = get_element(driver, 'xpath', "//input[@name='email']")
    username.send_keys('lijiahao')
    driver.switch_to.default_content()
    driver.quit()
