关键字从 hbhz_web/action/PageAction.py中调用
目前提供的关键字api：
打开浏览器：open_browser  -> 操作值：浏览器名字（不区分大小写）
浏览器窗口最大化：maximize_browser
打开网址：load_url -> 操作值：网址
强制等待：sleep -> 操作值：等待时间
清空输入的内容：clear -> 操作值1：定位方式  操作值2：元素位置
输入框中输入内容：input_value -> 操作值1：定位方式  操作值2：元素位置 操作值3：输入内容
点击操作：click_btn -> 操作值1：定位方式  操作值2：元素位置
获取当前页面的title：get_title
获取页面源码：get_page_source
切换到frame里面：switch_to_frame -> 操作值1：定位方式  操作值2：元素位置
跳到默认的frame：switch_to_default
模拟ctrl+v键：ctrl_v -> 操作值：想要粘贴的值
模拟tab键：tab_key
模拟enter键：enter_key
屏幕截图：save_screen_shot
显示等待页面元素出现在DOM中，不一定可见：wait_presence_of_element_located -> 操作值1：定位方式  操作值2：元素位置
检查frame是否存在，存在就切换到frame中：wait_frame_to_be_available_and_switch_to_it -> 操作值1：定位方式  操作值2：元素位置
显示等待页面元素出现在DOM中，并且可见：wait_visibility_of_element_located -> wait_visibility_of_element_located
关闭浏览器：quite_browser

随机数***************
1
输入拼接上的随机字符串的内容：input_random_str_value
-> 操作值1：定位方式  操作值2：元素位置 操作值3：固定内容 操作值4：拼接随机字符串长度
2
拼接上的范围内的数字：input_random_int_value
-> 操作值1：定位方式  操作值2：元素位置 操作值3：固定内容 操作值4：开始范围 操作值五：结束范围

断言***************
1
断言页面的title：assert_title -> 操作值：标题
2
断言目标字符串是否包含在页面源码中： assert_string_in_page_source -> 操作值：字符串
3
断言单条数据, 获取对应的字段的值：assert_mysql_first_value 
->
table:表名, 
key_data: 判断的字段名用[]传入，[字段1,字段2,字段3]
value_data: 判断的字段值用[]传入，[值1,值2,值3]
param: 你所要获取到字段的条件
param assert_value: 要断言的值
4
断言获取到符合条件数据的数量：assert_mysql_length
->
table: 表名
key_data: 判断的字段名用[]传入，[字段1,字段2,字段3]
value_data: 判断的字段值用[]传入，[值1,值2,值3]
assert_value: 预期长度
